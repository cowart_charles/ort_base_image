FROM ubuntu:18.04
RUN apt update
RUN apt install -y curl git unzip default-jdk virtualenv python3 python3-pip
RUN rm -f /usr/bin/python && ln -s /usr/bin/python3 /usr/bin/python
RUN rm -f /usr/bin/pip && ln -s /usr/bin/pip3 /usr/bin/pip
#RUN curl --insecure -o lianort.zip https://rivera.canvasslabs.com:5000/client/download/v1.3.1/linux
#RUN unzip lianort.zip
#RUN rm lianort.zip
#RUN git clone --recurse-submodules https://github.com/oss-review-toolkit/ort.git
#RUN cd ort && ./gradlew installDist --stacktrace
